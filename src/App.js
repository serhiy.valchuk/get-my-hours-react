import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import GetHours from './pages/GetHours/GetHours';
import LogHours from './pages/LogHours/LogHours';
import Login from './Login';
import PrivateRoute from './PrivateRoute';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './styles/App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: true
    };

    this.authenticate = this.authenticate.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  authenticate() {
    this.setState({
      isAuthenticated: true
    });
  }

  signOut() {
    this.setState({
      isAuthenticated: false
    });
  }

  render() {
    const { isAuthenticated } = this.state;

    return(
      <Router basename="/reactapp">
        <div className="App d-flex flex-column h-100">
          <div className="App__container flex-shrink-0">
            <Header isAuthenticated={isAuthenticated} onSignOut={this.signOut} />

            <PrivateRoute exact path="/" component={GetHours} isAuthenticated={isAuthenticated} />
            <PrivateRoute path="/log_hours" component={LogHours} isAuthenticated={isAuthenticated} />
            <Route path="/login" render={(props) => (<Login onAuthenticate={this.authenticate} {...props} />)} />
          </div>

          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
