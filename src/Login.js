import React from "react";
import {
  Redirect,
} from "react-router-dom";
import Alert from './components/Alert';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      redirectToReferrer: false 
    };

    this.login = this.login.bind(this);
  }

  login(e) {
    e.preventDefault();
    this.props.onAuthenticate();
    this.setState({
      redirectToReferrer: true
    });
  };

  render() {
    let { from } = this.props.location.state || { from: { pathname: "/" } };
    let { redirectToReferrer } = this.state;

    if (redirectToReferrer) return <Redirect to={from} />;

    const hint = (
      <p className="mb-0">If you want to get an access to this service, please, <a href="mailto:vitaliypodoba@gmail.com">Contact Us</a></p>
    )

    return (
      <main className="AppMain container py-3">
        <h1 className="mb-3">Login to access our service</h1>

        <Alert content={hint} />

        <form onSubmit={this.login}>
          <div className="form-group row">
            <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
            <div className="col-sm-8 col-lg-4">
              <input type="email" className="form-control" id="email" placeholder="Email" required />
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="password" className="col-sm-2 col-form-label">Password</label>
            <div className="col-sm-8 col-lg-4">
              <input type="password" className="form-control" id="password" placeholder="Password" required />
            </div>
          </div>

          <div className="form-group">
            <button type="submit" className="btn btn-primary">Login</button>
          </div>
        </form>
      </main>
    );
  }
}

export default Login;
