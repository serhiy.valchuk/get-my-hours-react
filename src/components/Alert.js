import React from 'react';

function Alert(props) {
  let status = 
    props.status !== undefined
      ? props.status
      : 'primary';

  return <div className={`alert alert-${status}`} role="alert">{props.content}</div>;
}

export default Alert;
