import React from 'react';

function Footer() {
  const year = new Date().getFullYear();

  return (
    <footer className="AppFooter mt-auto py-2">
      <div className="container">
        Get My Hours &copy; {year} <a href="mailto:vitaliypodoba@gmail.com">Contact Us</a>
      </div>
    </footer>
  )
}

export default Footer;
