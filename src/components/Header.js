import React from 'react';
import { NavLink } from "react-router-dom";
import ThemeSwitch from './ThemeSwitch';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    this.props.onSignOut();
  };

  render() {
    return (
      <header className="navbar navbar-expand sticky-top navbar-light bg-light px-0">
        <div className="container flex-wrap px-3">
          <NavLink to="/" className="navbar-brand">Get My Hours</NavLink>

          { this.props.isAuthenticated &&
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/">Get Hours</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/log_hours">Log Hours</NavLink>
              </li>
            </ul>
          }
  
          { this.props.isAuthenticated ? (
              <ul className="navbar-nav ml-auto align-items-center">
                <li className="nav-item">
                  {/* <button
                    type="button"
                    className="btn btn-link nav-link"
                    onClick={this.signOut}
                  >
                    Logout
                  </button> */}
                  <a className="nav-link" href="/logout">Logout</a>
                </li>
              </ul>
            ) : (
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link" href="mailto:vitaliypodoba@gmail.com">Request Invite</a>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">Login</NavLink>
                </li>
              </ul>
            )
          }

          <ThemeSwitch />
        </div>
      </header>
    );
  }
}

export default Header;
