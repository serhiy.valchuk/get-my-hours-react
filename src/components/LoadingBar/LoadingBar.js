import React from 'react';
import './LoadingBar.scss';
import { ReactComponent as TruckMonster } from '../../assets/svgs/truck-monster-solid.svg';

function LoadingBar() {
  return <div className="LoadingBar"><TruckMonster className="LoadingBar__icon" /></div>
}

export default LoadingBar;
