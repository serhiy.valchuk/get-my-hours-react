import React, { useState, useEffect } from 'react';

function ThemeSwitch() {
  const [active, isActive] = useState(localStorage.getItem('ThemeSwitch') === 'true');
  const css = `
  :root { filter: invert(100%); }
  .notInverted { filter: invert(100%) }`;

  useEffect(() => {
    localStorage.setItem('ThemeSwitch', active);
  });

  return (
    <>
      <button
        type="button"
        className="btn btn-dark ml-2"
        aria-pressed={active}
        onClick={() => isActive(!active)}
      >
        {active ? 'Light' : 'Dark'}
      </button>
      <style media={active ? 'screen' : 'none'}>
        {active ? css.trim() : this}
      </style>
    </>
  )
}

export default ThemeSwitch;
