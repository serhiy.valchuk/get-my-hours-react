import React from 'react';
import Alert from './../../components/Alert';
import GetHoursForm from './components/GetHoursForm';
import Services from './components/Services';
import TimeEntries from './components/TimeEntries';
import Calendar from './components/Calendar/Calendar';
import Ruler from './components/Ruler/Ruler';

class GetHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isInitialized: false,
      isLoading: false,
      isLoaded: false,
      site_services: [],
      services: [],
      timeEntries: [],
      totalHours: '',
      days: [],
      selectedMonth: '',
      selectedYear: '',
      selectedDay: ''
    };

    this.handleGetHoursData = this.handleGetHoursData.bind(this);
    this.handleSelectedDayChange = this.handleSelectedDayChange.bind(this);
  }

  componentDidMount() {
    fetch("/get_init_data.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isInitialized: true,
            site_services: result.site_services
          });
        },
        (error) => {
          this.setState({
            isInitialized: true,
            error
          });
        }
      )
  }

  handleGetHoursData(result, isLoading, isLoaded, selectedMonth, selectedYear) {
    this.setState({
      isLoading,
      isLoaded,
      selectedMonth,
      selectedYear,
      services: result.services,
      timeEntries: result.entries,
      totalHours: result.total,
      days: result.days,
      selectedDay: ''
    });
  }

  handleSelectedDayChange(day) {
    this.setState({
      selectedDay: day
    });
  }

  render() {
    const { error, isInitialized, isLoaded, isLoading, site_services, services, timeEntries, totalHours, days, selectedMonth, selectedYear, selectedDay } = this.state;
    const hint = (
      <React.Fragment>
        <p className="mb-0">Calendar displays your activity during selected month.</p>
        <p className="mb-0">Click on a cell to get hours report for that day.</p>
      </React.Fragment>
    )

    if (error) {
      return(
        <main className="AppMain container py-3">
          <Alert content="Can't get site services list. Please try later." status="danger" />
        </main>
      )
    } else if (!isInitialized) {
      return(
        <main className="AppMain container py-3">
          <Alert content="Loading..." />
        </main>
      )
    } else if (isInitialized) {
      return(
        <main className="AppMain container py-3">
          {site_services.length > 0 ? (
            <>
              <GetHoursForm site_services={site_services} onGetHours={this.handleGetHoursData} />

              {isLoaded &&
                <div className="getHours__results">
                  <div className="row mb-4">
                    <div className="col-md-auto">
                      <Calendar
                        days={days}
                        onSelectDay={this.handleSelectedDayChange}
                        selectedDay={selectedDay}
                        selectedMonth={selectedMonth}
                        selectedYear={selectedYear}
                      />

                      <Ruler />
                    </div>

                    <div className="col-md">
                      <Services services={services} />
                      <Alert content={hint} status="secondary" />
                    </div>
                  </div>

                  <TimeEntries timeEntries={timeEntries} totalHours={totalHours} selectedDay={selectedDay} />
                </div>
              }

              {!isLoaded && !isLoading &&
                <Alert content="Please, select month and year you want to check your logged hours for and click Get button." />
              }
            </>
          ) : (
            <div className="jumbotron">
              <h1 className="font-weight-normal mb-4">Ready to get started with effective hours spending?</h1>
              <p>Start by adding your first external project management system that we call a "Service".</p>
              <p>Once you add your services you'll be able to query hours and see your performance during whole month.</p>
              <p>Below you can see sample screen with hours reports from multiple Services</p>
              <p><a href="/add_service" className="btn btn-primary">Add Your First Service</a></p>
            </div>
          )}
        </main>
      )
    }
  }
}

export default GetHours;
