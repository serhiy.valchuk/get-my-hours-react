import React from 'react';
import DatePicker from "react-datepicker";
import { startOfMonth, lastDayOfMonth, setDate, getDate } from 'date-fns';
import enGB from "date-fns/locale/en-GB";

// import "react-datepicker/dist/react-datepicker.css";
import './Calendar.scss';

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: ''
    };
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect = date => {
    this.setState({
      startDate: date
    });
    const day = getDate(date)

    if (day === this.props.selectedDay) {
      this.setState({
        startDate: ''
      });
      this.props.onSelectDay('');
      return;
    }

    this.props.onSelectDay(day);
  };

  render() {
    const { days } = this.props;
    const daysWithHoursKeys = Object.keys(days);
    const selectedMonth = +this.props.selectedMonth - 1;
    const selectedYear = +this.props.selectedYear;
    const includedDates = daysWithHoursKeys.map((day) => setDate(new Date(selectedYear, selectedMonth), day));
    const dateClasses = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'];

    function setDateClasses(day) {
      if (days[day] <= 8) {
        return dateClasses[Math.round(days[day]) - 1]
      } else {
        return 'more';
      }
    }

    return (
      <DatePicker
        selected={this.state.startDate}
        onSelect={this.handleSelect}
        minDate={startOfMonth(new Date(selectedYear, selectedMonth))}
        maxDate={lastDayOfMonth(new Date(selectedYear, selectedMonth))}
        inline
        includeDates={includedDates}
        locale={enGB}
        dayClassName={date => {
            const dayNumber = getDate(date);
            return daysWithHoursKeys.includes(dayNumber.toString()) ? `react-datepicker__day--${setDateClasses(dayNumber)}` : undefined
          }
        }
      />
    );
  }
}

export default Calendar;
