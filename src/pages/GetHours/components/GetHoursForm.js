import React from 'react';
import Alert from './../../../components/Alert';
import LoadingBar from './../../../components/LoadingBar/LoadingBar';
import rangeRight from 'lodash/rangeRight';
import { subYears, getYear, format } from 'date-fns';

class GetHoursForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: false,
    };

    this.getHoursData = this.getHoursData.bind(this);
  }

  years = rangeRight(getYear(subYears(new Date(), 9)), getYear(new Date()) + 1, 1);
  months = Array.from(Array(12), (x, index) => {
    return {
      'id': format(new Date(25e8*(index+1)), 'MM'),
      'title': format(new Date(25e8*(index+1)), 'MMMM')
    }
  });

  getHoursData(event) {
    event.preventDefault();

    const month = event.target.month.value;
    const year = event.target.year.value;
    const service = event.target.service.value;

    this.setState({
      error: null,
      isLoading: true,
    });

    this.props.onGetHours([], true, false, month, year);

    fetch(`/get_hours.json?month=${month}&year=${year}&service=${service}`)
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoading: false
        });
        this.props.onGetHours(result, false, true, month, year);
      },
      (error) => {
        this.setState({
          isLoading: false,
          error
        });
      }
    )
  }

  render() {
    const { error, isLoading } = this.state;
    const site_services = this.props.site_services;
    const currentMonth = this.months.find(month => {
      const id = month.id;
      const currentDate = format(new Date(), 'MM');
      return currentDate === id;
    });

    return(
      <div className="GetHoursForm">
        <div className="row align-items-center mb-2">
          <div className="col-auto h2">Select month to get hours report for:</div>

          <form className="col-auto form-inline" onSubmit={this.getHoursData}>
            <label className="sr-only" htmlFor="year">Year</label>
            <select
              id="year"
              name="year"
              className="form-control my-2 mr-sm-2"
              disabled={isLoading}
            >
              {this.years.map((year, index) => (
                <option key={index} value={year}>{year}</option>
              ))}
            </select>

            <label className="sr-only" htmlFor="month">Month</label>
            <select
              id="month"
              name="month"
              className="form-control my-2 mr-sm-2"
              defaultValue={currentMonth.id}
              disabled={isLoading}
            >
              {this.months.map(month => (
                <option key={month.id} value={month.id}>{month.title}</option>
              ))}
            </select>

            <label className="sr-only" htmlFor="month">Service</label>
            <select
              id="service"
              name="service"
              className="form-control my-2 mr-sm-2 text-capitalize"
              disabled={isLoading}
            >
              {site_services.map(service => (
                <option key={service.title} value={service.title}>{service.title}</option>
              ))}
            </select>

            <button className="btn btn-primary" disabled={isLoading}>{isLoading ? 'Getting...' : 'Get'}</button>
          </form>
        </div>

        {isLoading && <LoadingBar />}

        {error && <Alert content="Can't get time entries. Please try later." status="danger" />}
      </div>
    )
  }
}

export default GetHoursForm;
