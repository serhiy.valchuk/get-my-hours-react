import React from 'react';
import './Ruler.scss';

function Ruler () {
  return(
    <ul className="Ruler d-flex list-unstyled">
      <li className="Ruler__item Ruler__item--one">1</li>
      <li className="Ruler__item Ruler__item--two">2</li>
      <li className="Ruler__item Ruler__item--three">3</li>
      <li className="Ruler__item Ruler__item--four">4</li>
      <li className="Ruler__item Ruler__item--five">5</li>
      <li className="Ruler__item Ruler__item--six">6</li>
      <li className="Ruler__item Ruler__item--seven">7</li>
      <li className="Ruler__item Ruler__item--eight">8</li>
      <li className="Ruler__item Ruler__item--more">&gt;8</li>
    </ul>
  )
}

export default Ruler;
