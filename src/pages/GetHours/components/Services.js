import React from 'react';
import { ReactComponent as Success } from '../../../assets/svgs/check-square-solid.svg';
import { ReactComponent as Error } from '../../../assets/svgs/exclamation-triangle-solid.svg';

class Services extends React.Component {
  render() {
    return(
      <ul className="list-group mb-3">
        {this.props.services.map(service => (
          <li className="list-group-item" key={service.id}>
            {service.error ? (
              <div>
                <span className="badge badge-success mr-2">{service.type}</span>
                <span className="mr-2">&raquo;</span>
                <span className="font-weight-bold mr-2">{service.title}:</span>
                <span className="text-danger mr-2">
                  <Error className="notInverted" width="14" height="16" />
                </span>
                <span className="mr-2">Reason: {service.error}</span>
              </div>
            ) : (
              <div>
                <span className="badge badge-success mr-2">{service.type}</span>
                <span className="mr-2">&raquo;</span>
                <span className="font-weight-bold mr-2">{service.title}:</span>
                <span className="text-success mr-2">
                  <Success className="notInverted" width="18" height="16" />
                </span>
                <span className="mr-2">Hours: {service.data.hours.toFixed(2)},</span>
                <span className="mr-2">Tasks: {service.data.tasks},</span>
                <span className="mr-2">Time Entries: {service.data.entries}</span>
              </div>
            )}
          </li>
        ))}
      </ul>
    )
  }
}

export default Services;
