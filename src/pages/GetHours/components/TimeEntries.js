import React from 'react';
import ResultsTable from './TimeEntriesTable';
import Alert from './../../../components/Alert';

class TimeEntries extends React.Component {
  render() {
    const timeEntries =
      this.props.selectedDay ?
      this.props.timeEntries.filter(timeEntry => {
        return +timeEntry.day === this.props.selectedDay
      }) :
      this.props.timeEntries;

    const currentTotalHours = timeEntries.reduce((accumulator, timeEntry) => {return accumulator + +timeEntry.hours}, 0).toFixed(2);

    return(
      <div className="TimeEntries">
        {+this.props.totalHours ? (
          <section className="timeEntries">
            <header className="font-weight-bold text-right pb-2">
              Total Month Hours: {this.props.totalHours.toFixed(2)}
            </header>

            <ResultsTable timeEntries={timeEntries} currentTotalHours={currentTotalHours} />
          </section>
        ) : (
          <Alert content="No time entries" status="warning" />
        )}
      </div>
    )
  }
}

export default TimeEntries;
