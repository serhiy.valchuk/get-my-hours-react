import React from 'react';

class TimeEntriesTable extends React.Component {
  render() {
    return(
      <div className="table-responsive">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Date</th>
              <th scope="col">Person</th>
              <th scope="col">Description</th>
              <th scope="col">Hours</th>
            </tr>
          </thead>
          <tbody>
            {this.props.timeEntries.map((timeEntry, index) => (
              <tr key={index}>
                <td className="text-nowrap">{timeEntry.date}</td>
                <td className="text-nowrap">{timeEntry.person}</td>
                <td className="w-100">
                  <div className="text-nowrap">
                    <span className="badge badge-success mr-2">{timeEntry.service}</span>
                    <span className="mr-2">&raquo;</span>
                    <span className="font-weight-bold mr-2">{timeEntry.client}:</span>
                    <span className="mr-2">&raquo;</span>
                    <a href={timeEntry.project_url} target="_blank" rel="noopener noreferrer" className="mr-2">{timeEntry.project}</a>
                    <span className="mr-2">&raquo;</span>
                    <a href={timeEntry.task_url} target="_blank" rel="noopener noreferrer">{timeEntry.task}</a>
                  </div>
                  <div>{timeEntry.description}</div>
                </td>
                <td className="text-right">{timeEntry.hours}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <th scope="col" colSpan="3">Total displayed hours:</th>
              <td className="text-right">
                <strong>{this.props.currentTotalHours}</strong>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    )
  }
}

export default TimeEntriesTable;
