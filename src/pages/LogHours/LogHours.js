import React from 'react';
import Alert from './../../components/Alert';

class LogHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isInitialized: false,
      isProjectsLoading: false,
      services: [],
      projects: [],
    };
    this.handleService = this.handleService.bind(this);
  }

  componentDidMount() {
    fetch("/get_services.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isInitialized: true,
            services: result.services
          });
        },
        (error) => {
          this.setState({
            isInitialized: true,
            error
          });
        }
      )
  }

  handleService(event) {
    this.setState({
      isProjectsLoading: true,
    });

    fetch(`/get_projects.json?service=${event.target.value}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isProjectsLoading: true,
            projects: result.projects
          });
        },
        (error) => {
          this.setState({
            isProjectsLoading: true,
            error
          });
        }
      )
  }

  render() {
    // const { error, isInitialized, services, projects } = this.state;
    const { error, isInitialized } = this.state;
    // const subProjects = subProject => {
    //   if (subProjects.length === 0) {
    //     return;
    //   }

    //   return subProject.map(project => (
    //     <React.Fragment key={project.id}>
    //       <option key={project.id} value={project.id} className="text-muted">{project.name}</option>
    //       subProjects(subProject)
    //     </React.Fragment>
    //   ));
    // }

    if (error) {
      return(
        <main className="AppMain container py-3">
          <Alert content={error.message} status="danger" />
        </main>
      )
    } else if (!isInitialized) {
      return(
        <main className="AppMain container py-3">
          <Alert content="Loading..." />
        </main>
      )
    } else if (isInitialized) {
      return(
        <main className="AppMain container py-3">
          <h1 className="mb-4">Here you can Track and Log your working hours</h1>

          <Alert content="This page is under construction" />

          {/* <section className="mb-4">
            <h2>Pick Existing Task:</h2>

            <form className="form-inline">
              <label className="sr-only" htmlFor="year">Service</label>
              <select
                id="service"
                name="service"
                className="form-control my-2 mr-sm-2"
                onChange={this.handleService}
                value={this.state.value}
              >
                <option value="">Select Service...</option>
                {services.map(service => (
                  <option key={service.id} value={service.id}>{service.name}</option>
                ))}
              </select>

              {projects.length > 0 &&
                <React.Fragment>
                  <label className="sr-only" htmlFor="project">Project</label>
                  <select id="project" name="project" className="form-control my-2 mr-sm-2">
                    <option value="">Select Project...</option>
                    {projects.map(project => (
                      <React.Fragment key={project.id}>
                        <option key={project.id} value={project.id} className="font-weight-bold">{project.name}</option>
                        {subProjects(project.subprojects)}
                      </React.Fragment>
                      ))}
                  </select>
                </React.Fragment>
              }
            </form>
          </section>

          <section>
            <h2>Pending Time Entries:</h2>
          </section> */}
        </main>
      )
    }
  }
}

export default LogHours;
